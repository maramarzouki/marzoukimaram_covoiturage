class Ride{
    constructor(date, departure, arrival){
        this.date = date;
        this.departure = departure;
        this.arrival = arrival;
    }

    get date(){
        return this.date;
    }
}