class Driver{
    constructor(name, userSince){
        this.name = name;
        this.userSince = userSince;
    }


    get userSince(){
        return this.userSince;
    }
}